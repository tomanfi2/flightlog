package eu.profinit.education.flightlog.service;

import com.sun.jdi.event.ExceptionEvent;
import eu.profinit.education.flightlog.domain.entities.Flight;
import eu.profinit.education.flightlog.domain.repositories.FlightRepository;
import eu.profinit.education.flightlog.exceptions.FlightLogException;
import eu.profinit.education.flightlog.to.FileExportTo;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Service
public class CsvExportServiceImpl implements CsvExportService {

    private final FlightRepository flightRepository;

    private final String fileName;

    public CsvExportServiceImpl(FlightRepository flightRepository, @Value("${csv.export.flight.fileName}") String fileName) {
        this.flightRepository = flightRepository;
        this.fileName = fileName;
    }

    @Override
    public FileExportTo getAllFlightsAsCsv() {
        // TODO 4.3: Naimplementujte vytváření CSV.
        // Tip: můžete použít Apache Commons CSV - https://commons.apache.org/proper/commons-csv/ v příslušných pom.xml naleznete další komentáře s postupem
        List<Flight> flights = flightRepository.findAll(Sort.by(
            Sort.Order.asc("takeoffTime"),
            Sort.Order.asc("id")
        ));

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy hh:mm ss");

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Writer printWriter = new OutputStreamWriter(baos, StandardCharsets.UTF_8);
        try {
            CSVPrinter csvExport = new CSVPrinter(printWriter, CSVFormat.DEFAULT);

            csvExport.printRecord( "Typ", "Imatrikulace", "Osádka", "Úkol", "Start", "Přistání", "Doba letu (min)");
            for (Flight flight : flights) {

                Long flightLengthInMinutes = null;
                if(flight.getTakeoffTime() != null && flight.getLandingTime() != null) {
                    flightLengthInMinutes = ChronoUnit.MINUTES.between(flight.getTakeoffTime(), flight.getLandingTime());
                }

                csvExport.printRecord(
                    flight.getFlightType().toString(),
                    flight.getAirplane().getSafeImmatriculation(),
                    (flight.getPilot() != null ?
                        flight.getPilot().getFirstName() +  (flight.getCopilot() != null ? " " + flight.getCopilot().getFullName() + "" : "")
                        : ""),
                    flight.getTask().getValue(),
                    flight.getTakeoffTime() != null ? flight.getTakeoffTime().format(formatter) : "",
                    flight.getLandingTime() != null ? flight.getLandingTime().format(formatter) : "",
                    flightLengthInMinutes != null ? flightLengthInMinutes : ""
                );
            }
            csvExport.flush();
            return new FileExportTo(this.fileName, MediaType.valueOf("text/csv"), baos.toByteArray());
        } catch (IOException e) {
            throw new FlightLogException("Error during generating flights CSV report.", e);
        }
    }

}
