package eu.profinit.education.flightlog.to;

import eu.profinit.education.flightlog.domain.entities.Flight;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class FlightTuppleTo {
    private FlightTo towplane;
    private FlightTo glider;

    public static FlightTuppleTo fromEntities(Flight a, Flight b) {
        if (a == null || b == null) {
            return null;
        }
        return new FlightTuppleTo(FlightTo.fromEntity(a), FlightTo.fromEntity(b));
    }
}
