package eu.profinit.education.flightlog.rest;

import eu.profinit.education.flightlog.domain.entities.Airplane;
import eu.profinit.education.flightlog.service.AirplaneService;
import eu.profinit.education.flightlog.to.AirplaneTo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@Slf4j
public class AirplaneController {

    private final AirplaneService airplaneService;

    // TODO 3.1: Vystavte REST endpoint vracející seznam klubových letadel
    // letadla získáte voláním AirplaneService
    // bude se volat metoda GET na /airplane
    // struktura odpovědi je dána objektem AirplaneTo

    @GetMapping("/airplane")
    List<AirplaneTo> getClubAirplanes(){
        List<AirplaneTo> clubAirplanes = airplaneService.getClubAirplanes();
        log.debug("Flights in the air:\n{}", clubAirplanes);
        return clubAirplanes;
    }

}
